package de.fsdev.discogsmusic.page.artist


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.squareup.picasso.Picasso
import de.fsdev.discogsmusic.R
import de.fsdev.discogsmusic.network.data.DetailArtist
import de.fsdev.discogsmusic.network.data.Group
import de.fsdev.discogsmusic.network.service.ApiFactory
import kotlinx.android.synthetic.main.fragment_artist.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ArtistFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_artist, container, false)
    }

    override fun onStart() {
        super.onStart()

        val artistId : Int? = arguments?.getInt("artist_id")
        Log.d("ArtistFragment", "artist_id: "+artistId)

        artistId?.let {
            val discogsService = ApiFactory.discogsApi

            GlobalScope.launch(Dispatchers.Main) {
                val postRequest = discogsService.getDetailArtist(artistId.toString())
                try {
                    val response = postRequest.await()
                    if (response.isSuccessful){
                        val posts = response.body()
                        draw(posts)

                    }else{
                        Log.d("MainActivity ",response.errorBody().toString())
                    }

                }catch (e: Exception){

                }
            }
        }
    }

    private fun draw(detailArtist: DetailArtist?) {
        detailArtist?.let {
            Log.d("ArtistFragment", "draw artistName: "+detailArtist.name)

            val picasso = Picasso.get()
            picasso.load(detailArtist.images[0]?.uri)
                .into(artistImage)

            artist?.text = "Artist: " + detailArtist.name
            realname?.text = "Realname: " + detailArtist.realname
            profile?.text = "Profile: " + detailArtist.profile

            val sbGroups = StringBuilder()
            for (g: Group in detailArtist.groups) {
                sbGroups.append(g.name).append(", ")
            }
            sbGroups.delete(sbGroups.lastIndex-1, sbGroups.lastIndex)
            groups?.text = "Groups: " + sbGroups.toString()
        }
    }
}

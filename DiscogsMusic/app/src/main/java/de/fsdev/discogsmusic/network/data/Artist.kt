package de.fsdev.discogsmusic.network.data

data class DetailArtist(
    val data_quality: String,
    val groups: List<Group>,
    val id: Int,
    val images: List<ArtistImage>,
    val name: String,
    val namevariations: List<String>,
    val profile: String,
    val realname: String,
    val releases_url: String,
    val resource_url: String,
    val uri: String,
    val urls: List<String>
)

data class Group(
    val active: Boolean,
    val id: Int,
    val name: String,
    val resource_url: String,
    val thumbnail_url: String
)

data class ArtistImage(
    val height: Int,
    val resource_url: String,
    val type: String,
    val uri: String,
    val uri150: String,
    val width: Int
)
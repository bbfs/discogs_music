package de.fsdev.discogsmusic.page.album


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.squareup.picasso.Picasso
import de.fsdev.discogsmusic.R
import de.fsdev.discogsmusic.network.data.*
import de.fsdev.discogsmusic.network.service.ApiFactory
import kotlinx.android.synthetic.main.fragment_album_detail.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class AlbumDetailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_album_detail, container, false)
    }

    override fun onStart() {
        super.onStart()

        val releaseId : Int? = arguments?.getInt("release_id")
        Log.d("AlbumDetailFragment", "release_id: "+releaseId)

        releaseId?.let {
            val discogsService = ApiFactory.discogsApi

            GlobalScope.launch(Dispatchers.Main) {
                val postRequest = discogsService.getDetailRelease(releaseId.toString())
                try {
                    val response = postRequest.await()
                    if (response.isSuccessful){
                        val posts = response.body()
                        draw(posts)

                    }else{
                        Log.d("MainActivity ",response.errorBody().toString())
                    }

                }catch (e: Exception){

                }
            }
        }
    }

    private fun draw(release: DetailRelease?) {
        release?.let {

            val picasso = Picasso.get()
            picasso.load(release.images[0]?.uri)
                    .into(albumImage)

            val sbArtists = StringBuilder()
            for (a: ReleaseArtist in release.artists) {
                sbArtists.append(a.name).append(", ")
            }
            sbArtists.delete(sbArtists.lastIndex-1, sbArtists.lastIndex)

            Log.d("AlbumDetailFragment", "draw artists: " + sbArtists.toString())
            artist?.text = "Artists: " + sbArtists.toString()

            title?.text = "Title: " + release.title

            val sbLabels = StringBuilder()
            for (a: ReleaseLabel in release.labels) {
                sbLabels.append(a.name).append(", ")
            }
            sbLabels.delete(sbLabels.lastIndex-1, sbLabels.lastIndex)
            label?.text = "Labels: " + sbLabels.toString()

            val sbFormats = StringBuilder()
            for (a: ReleaseFormat in release.formats) {
                sbFormats.append(a.name).append(", ")
            }
            sbFormats.delete(sbFormats.lastIndex-1, sbFormats.lastIndex)
            format?.text = "Formats: " + sbFormats.toString()

            land?.text = "Country: " + release.country
            releaseDate?.text = "Released: " + release.released_formatted

            val sbGenres = StringBuilder()
            for (a: String in release.genres) {
                sbGenres.append(a).append(", ")
            }
            sbGenres.delete(sbGenres.lastIndex-1, sbGenres.lastIndex)
            genre?.text = "Genres: " + sbGenres.toString()

            val sbStyles = StringBuilder()
            for (a: String in release.styles) {
                sbStyles.append(a).append(", ")
            }
            sbStyles.delete(sbStyles.lastIndex-1, sbStyles.lastIndex)
            style?.text = "Styles: " + sbStyles.toString()

            button_artist?.setOnClickListener { openArtistPage(release.artists[0].id) }
        }
    }

    private fun openArtistPage(artistId : Int) {
        var bundle = bundleOf("artist_id" to artistId)
        findNavController().navigate(R.id.action_albumDetailFragment_to_artistFragment, bundle)
    }
}

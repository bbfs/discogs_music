package de.fsdev.discogsmusic.network.service

import de.fsdev.discogsmusic.network.data.DetailArtist
import de.fsdev.discogsmusic.network.data.DetailRelease
import de.fsdev.discogsmusic.network.data.UserReleases
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface DiscogsApi{

    @GET("users/{user}/collection/folders/0/releases")
    fun getUserReleases(@Path("user") user : String): Deferred<Response<UserReleases>>

    @GET("releases/{releaseId}")
    fun getDetailRelease(@Path("releaseId") releaseId : String): Deferred<Response<DetailRelease>>

    @GET("artists/{artistId}")
    fun getDetailArtist(@Path("artistId") artistId : String): Deferred<Response<DetailArtist>>
}
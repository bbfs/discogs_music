package de.fsdev.discogsmusic.network.data

data class UserReleases(
    val pagination: Pagination,
    val releases: List<Release>
)

data class Release(
    val basic_information: BasicInformation,
    val date_added: String,
    val folder_id: Int,
    val id: Int,
    val instance_id: Int,
    val rating: Int
)

data class BasicInformation(
    val artists: List<Artist>,
    val cover_image: String,
    val formats: List<Format>,
    val id: Int,
    val labels: List<Label>,
    val master_id: Int,
    val master_url: String,
    val resource_url: String,
    val thumb: String,
    val title: String,
    val year: Int
)

data class Format(
    val descriptions: List<String>,
    val name: String,
    val qty: String
)

data class Label(
    val catno: String,
    val entity_type: String,
    val entity_type_name: String,
    val id: Int,
    val name: String,
    val resource_url: String
)

data class Artist(
    val anv: String,
    val id: Int,
    val join: String,
    val name: String,
    val resource_url: String,
    val role: String,
    val tracks: String
)

data class Pagination(
    val items: Int,
    val page: Int,
    val pages: Int,
    val per_page: Int,
    val urls: Urls
)

class Urls(
)
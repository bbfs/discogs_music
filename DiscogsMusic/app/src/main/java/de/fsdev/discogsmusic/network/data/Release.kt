package de.fsdev.discogsmusic.network.data

data class DetailRelease(
    val artists: List<ReleaseArtist>,
    val artists_sort: String,
    val community: Community,
    val companies: List<Company>,
    val country: String,
    val data_quality: String,
    val date_added: String,
    val date_changed: String,
    val estimated_weight: Int,
    val extraartists: List<Any>,
    val format_quantity: Int,
    val formats: List<ReleaseFormat>,
    val genres: List<String>,
    val id: Int,
    val identifiers: List<Identifier>,
    val images: List<Image>,
    val labels: List<ReleaseLabel>,
    val lowest_price: Double,
    val master_id: Int,
    val master_url: String,
    val notes: String,
    val num_for_sale: Int,
    val released: String,
    val released_formatted: String,
    val resource_url: String,
    val series: List<Any>,
    val status: String,
    val styles: List<String>,
    val thumb: String,
    val title: String,
    val tracklist: List<Tracklist>,
    val uri: String,
    val year: Int
)

data class ReleaseArtist(
    val anv: String,
    val id: Int,
    val join: String,
    val name: String,
    val resource_url: String,
    val role: String,
    val thumbnail_url: String,
    val tracks: String
)

data class Tracklist(
    val duration: String,
    val extraartists: List<Extraartist>,
    val position: String,
    val title: String,
    val type_: String
)

data class Extraartist(
    val anv: String,
    val id: Int,
    val join: String,
    val name: String,
    val resource_url: String,
    val role: String,
    val thumbnail_url: String,
    val tracks: String
)

data class Community(
    val contributors: List<Contributor>,
    val data_quality: String,
    val have: Int,
    val rating: Rating,
    val status: String,
    val submitter: Submitter,
    val want: Int
)

data class Contributor(
    val resource_url: String,
    val username: String
)

data class Submitter(
    val resource_url: String,
    val username: String
)

data class Rating(
    val average: Double,
    val count: Int
)

data class Company(
    val catno: String,
    val entity_type: String,
    val entity_type_name: String,
    val id: Int,
    val name: String,
    val resource_url: String,
    val thumbnail_url: String
)

data class ReleaseLabel(
    val catno: String,
    val entity_type: String,
    val entity_type_name: String,
    val id: Int,
    val name: String,
    val resource_url: String,
    val thumbnail_url: String
)

data class Image(
    val height: Int,
    val resource_url: String,
    val type: String,
    val uri: String,
    val uri150: String,
    val width: Int
)

data class Identifier(
    val description: String,
    val type: String,
    val value: String
)

data class ReleaseFormat(
    val descriptions: List<String>,
    val name: String,
    val qty: String
)
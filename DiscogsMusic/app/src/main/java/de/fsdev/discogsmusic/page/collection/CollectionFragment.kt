package de.fsdev.discogsmusic.page.collection


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import de.fsdev.discogsmusic.Constants
import de.fsdev.discogsmusic.R
import de.fsdev.discogsmusic.network.data.Artist
import de.fsdev.discogsmusic.network.data.Release
import de.fsdev.discogsmusic.network.data.UserReleases
import de.fsdev.discogsmusic.network.service.ApiFactory
import kotlinx.android.synthetic.main.fragment_collection.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class CollectionFragment : Fragment() {

    val releases: ArrayList<Release> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_collection, container, false)
    }

    override fun onStart() {
        super.onStart()

        val discogsService = ApiFactory.discogsApi

        GlobalScope.launch(Dispatchers.Main) {
            val postRequest = discogsService.getUserReleases(Constants.USER_NAME)
            try {
                val response = postRequest.await()
                if(response.isSuccessful){
                    val posts = response.body()
                    drawReleases(posts)

                }else{
                    Log.d("MainActivity ",response.errorBody().toString())
                }

            }catch (e: Exception){

            }
        }
    }

    private fun drawReleases(ur: UserReleases?) {
        releases.clear()
        ur?.let {
            for (r: Release in ur.releases) {
                releases.add(r)
                for (a: Artist in r.basic_information.artists) {
                    Log.d("drawReleases", a.name)
                }
            }

            val adapter = ReleaseListAdapter(releases)

            adapter.setOnItemClickListener(object : ReleaseListAdapter.ClickListener {
                override fun onClick(pos: Int, aView: View) {
                    Log.d("onClick", "pos: "+pos.toString() +", releaseId: "+releases[pos].id)
                    openDetailPage(releases[pos].id)
                }
            })

            release_list?.adapter = adapter
        }
    }

    private fun openDetailPage(releaseId : Int) {
        var bundle = bundleOf("release_id" to releaseId)
        findNavController().navigate(R.id.action_collectionFragment_to_albumDetailFragment, bundle)
    }
}

package de.fsdev.discogsmusic.page.collection

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import de.fsdev.discogsmusic.R
import de.fsdev.discogsmusic.network.data.Artist
import de.fsdev.discogsmusic.network.data.Release
import kotlinx.android.synthetic.main.release_list_item.view.*

class ReleaseListAdapter (val items : ArrayList<Release>) : RecyclerView.Adapter<ReleaseListAdapter.MyViewHolder>() {

    lateinit var mClickListener: ClickListener

    fun setOnItemClickListener(aClickListener: ClickListener) {
        mClickListener = aClickListener
    }

    interface ClickListener {
        fun onClick(pos: Int, aView: View)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.release_list_item, parent, false))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val release = items.get(position)

        val sbArtists = StringBuilder()
        for (a: Artist in release.basic_information.artists) {
            sbArtists.append(a.name).append(", ")
        }
        sbArtists.delete(sbArtists.lastIndex-1, sbArtists.lastIndex)

        holder?.tvArtist?.text = sbArtists.toString()
        holder?.tvTitle?.text = release.basic_information.title
    }

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return items.size
    }

    inner class MyViewHolder (view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            mClickListener.onClick(adapterPosition, v)
        }

        val tvArtist = view.artist
        val tvTitle = view.title
    }
}


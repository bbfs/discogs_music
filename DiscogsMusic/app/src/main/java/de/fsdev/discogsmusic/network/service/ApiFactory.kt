package de.fsdev.discogsmusic.network.service

import de.fsdev.discogsmusic.Constants


object ApiFactory{

    val discogsApi : DiscogsApi = RetrofitFactory.retrofit(Constants.DISCOGS_BASE_URL)
        .create(DiscogsApi::class.java)
}